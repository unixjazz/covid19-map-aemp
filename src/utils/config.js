// name of AEMP's CARTO account
export const aempCartoAccount = "ko";

// table in CARTO that syncs with the tenants protections legislation Google sheet
export const cartoSheetSyncTable = "public.upkkvli6yiin4ezcx7sprg";

// table in CARTO that syncs with the housing actions Google sheet
export const cartoHousingActionsTable = "public.fpif0ltzsdosq5sbpplmbw";

// misc admin boundary geometry tables
export const cartoStatesTable = "public.states_and_provinces_global";
export const cartoNationsTable = "public.countries";
